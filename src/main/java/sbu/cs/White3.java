package sbu.cs;

public class White3 implements WhiteFunction{
    //3 حرف ها را ازاخر یکی در میان بین هم قرار میدهد.
    public String func(String first, String second){
        if (first.length() != second.length())
            return NotEqual(first , second);
        else
            return Equal(first, second);
    }
    private String NotEqual(String first, String second){
        StringBuilder main = new StringBuilder();
        int size = first.length() < second.length() ? first.length() : second.length();
        int bigSize = first.length() < second.length() ? second.length() : first.length();
        String bigger = first.length() < second.length() ? second : first;
        int j = 0;
        int p = second.length() - 1;
        for (int i = 0; i < 2 * size; i++){
            if (i % 2 == 0){
                main.append(first.charAt(j));
                j++;
            } else {
                main.append(second.charAt(p));
                p--;
            }
        }
        for (int k = bigSize - size - 1; k > -1; k--){
            main.append(bigger.charAt(k));
        }
        return main.toString();
    }
    private String Equal(String first, String second){
        StringBuilder main = new StringBuilder();

        int j = 0;
        int p = first.length() - 1;
        for (int i = 0; i < 2 * first.length(); i++){
            if (i % 2 == 0){
                main.append(first.charAt(j));
                j++;
            } else {
                main.append(second.charAt(p));
                p--;
            }
        }
        return main.toString();
    }
}
