package sbu.cs;

public class White4 implements WhiteFunction{
    //4 رشته با سایز زوج را بر میگرداند.
    public String func(String first, String second){
        if (first.length() % 2 == 0)
            return first;
        return second;
    }
}
