package sbu.cs;

public class Blue extends Colors{
    private String leftIn;
    private String upIn;
    private BlackFunction BF;
    public void setLeftInputBlue(String leftIn){
        this.leftIn = leftIn;
    }
    public void setUpInputBlue(String upIn){
        this.upIn = upIn;
    }
    public String getGoDownBlue(BlackFunction BF){
        this.BF = BF;
        String goDown = BF.func(this.upIn);
        return goDown;
    }
    public String getGoRightBlue(BlackFunction BF){
        this.BF = BF;
        String goRight = BF.func(this.leftIn);
        return goRight;
    }
}
