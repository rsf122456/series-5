package sbu.cs;

public class Pink extends Colors{
    private String FirstInput;
    private String SecondInput;
    private WhiteFunction WF;
    public void setFirstInputPink(String FirstInput){
        this.FirstInput = FirstInput;
    }
    public void setSecondInputPink(String SecondInput){
        this.SecondInput = SecondInput;
    }
    public String getLastOutputPink(WhiteFunction WF){
        this.WF = WF;
        String LastOutput = WF.func(this.FirstInput,this.SecondInput);
        return LastOutput;
    }
}
