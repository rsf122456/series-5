package sbu.cs;

public class Green extends Colors{
    private String GreenInput;
    private BlackFunction BF;
    public void setGreenInput(String GreenInput){
        this.GreenInput = GreenInput;
    }
    public String getGreenOutput(BlackFunction BF){
        this.BF = BF;
        String GreenOutput = BF.func(this.GreenInput);
        return GreenOutput;
    }
}
