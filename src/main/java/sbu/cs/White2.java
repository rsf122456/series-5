package sbu.cs;

public class White2 implements WhiteFunction{
    //2 رشته دوم را برعکس میکند و به انتهای رشته اول بر میگرداند.
    public String func(String first, String second){
        StringBuilder Revers = new StringBuilder(second);
        return first + Revers.reverse().toString();
    }
}
