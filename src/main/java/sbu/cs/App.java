package sbu.cs;

public class App {
    private int[][] arr;
    private int n;
    private String input;
    private Colors[][] colors = new Colors[n][n];
    /**
     * use this function for magical machine question.
     *
     * @param n     size of machine
     * @param arr   an array in size n * n
     * @param input the input string
     * @return the output string of machine
     */
    public String main(int n, int[][] arr, String input) {
        this.n = n;
        this.arr = arr;
        this.input = input;
        if (n == 2){
            return setRowsEqual2();
        } else if (n == 3){
            return setRowsEqual3();
        } else {
            return setRowsBiggerThan3();
        }
    }
    private BlackFunction chooseBlackFunc(int choose){
        switch (choose){
            case 1 : return new Black1();
            case 2 : return new Black2();
            case 3 : return new Black3();
            case 4 : return new Black4();
            case 5 : return new Black5();
            default : return null;
        }
    }
    private WhiteFunction chooseWhiteFunc(int choose){
        switch (choose){
            case 1 : return new White1();
            case 2 : return new White2();
            case 3 : return new White3();
            case 4 : return new White4();
            case 5 : return new White5();
            default : return null;
        }
    }
    private String setRowsEqual2(){
        ((Green) colors[0][0]).setGreenInput(input);
        ((Yellow) colors[0][n - 1]).setInputYellow(((Green) colors[0][n - 2]).
                getGreenOutput(chooseBlackFunc(arr[0][n - 2])));
        ((Yellow) colors[n - 1][0]).setInputYellow(((Green) colors[n - 2][0]).
                getGreenOutput(chooseBlackFunc(arr[n - 2][0])));
        ((Pink) colors[n - 1][n - 1]).setFirstInputPink(((Yellow) colors[n - 1][0]).
                getOutputYellow(chooseBlackFunc(arr[n - 1][0])));
        ((Pink) colors[n - 1][n - 1]).setSecondInputPink(((Yellow) colors[0][n - 1]).
                getOutputYellow(chooseBlackFunc(arr[0][n - 1])));
        return ((Pink) colors[n - 1][n - 1]).getLastOutputPink(chooseWhiteFunc(arr[n - 1][n - 1]));
    }
    private String setRowsEqual3(){
        ((Green) colors[0][0]).setGreenInput(input);
        for (int i = 1; i < n - 1; i++){
            ((Green) colors[0][i]).setGreenInput(((Green) colors[0][i - 1]).
                    getGreenOutput(chooseBlackFunc(arr[0][i - 1])));
        }
        ((Yellow) colors[0][n - 1]).setInputYellow(((Green) colors[0][n - 2]).
                getGreenOutput(chooseBlackFunc(arr[0][n - 2])));
        for (int i = 1; i < n - 1; i++){
            ((Green) colors[i][0]).setGreenInput(((Green) colors[i - 1][0]).
                    getGreenOutput(chooseBlackFunc(arr[i - 1][0])));
        }
        ((Yellow) colors[n - 1][0]).setInputYellow(((Green) colors[n - 2][0]).
                getGreenOutput(chooseBlackFunc(arr[n - 2][0])));
        ((Blue) colors[1][1]).setLeftInputBlue(((Green) colors[1][0]).
                getGreenOutput(chooseBlackFunc(arr[1][0])));
        ((Blue) colors[1][1]).setUpInputBlue(((Green) colors[0][1]).
                getGreenOutput(chooseBlackFunc(arr[0][1])));
        ((Pink) colors[1][n - 1]).setFirstInputPink(((Blue) colors[1][n - 2]).
                getGoRightBlue(chooseBlackFunc(arr[1][n - 2])));
        ((Pink) colors[1][n - 1]).setSecondInputPink(((Yellow) colors[0][n - 1]).
                getOutputYellow(chooseBlackFunc(arr[0][n - 1])));
        ((Pink) colors[n - 1][1]).setFirstInputPink(((Yellow) colors[n - 1][0]).
                getOutputYellow(chooseBlackFunc(arr[n - 1][0])));
        ((Pink) colors[n - 1][1]).setSecondInputPink(((Blue) colors[n - 2][1]).
                getGoDownBlue(chooseBlackFunc(arr[n - 2][1])));
        ((Pink) colors[n - 1][n - 1]).setFirstInputPink(((Pink) colors[n - 1][n - 2]).
                getLastOutputPink(chooseWhiteFunc(arr[n - 1][n - 2])));
        ((Pink) colors[n - 1][n - 1]).setSecondInputPink(((Pink) colors[n - 2][n - 1]).
                getLastOutputPink(chooseWhiteFunc(arr[n - 2][n - 1])));
        return ((Pink) colors[n - 1][n - 1]).getLastOutputPink(chooseWhiteFunc(arr[n - 1][n - 1]));
    }
    private String setRowsBiggerThan3(){
        //خط صفرم جدول افقی
        ((Green) colors[0][0]).setGreenInput(input);
        for (int i = 1; i < n - 1; i++){
            ((Green) colors[0][i]).setGreenInput(((Green) colors[0][i - 1]).
                    getGreenOutput(chooseBlackFunc(arr[0][i - 1])));
        }
        ((Yellow) colors[0][n - 1]).setInputYellow(((Green) colors[0][n - 2]).
                getGreenOutput(chooseBlackFunc(arr[0][n - 2])));
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        //ستون صفرم جدول
        for (int i = 1; i < n - 1; i++){
            ((Green) colors[i][0]).setGreenInput(((Green) colors[i - 1][0]).
                    getGreenOutput(chooseBlackFunc(arr[i - 1][0])));
        }
        ((Yellow) colors[n - 1][0]).setInputYellow(((Green) colors[n - 2][0]).
                getGreenOutput(chooseBlackFunc(arr[n - 2][0])));
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        ((Blue) colors[1][1]).setLeftInputBlue(((Green) colors[1][0]).
                getGreenOutput(chooseBlackFunc(arr[1][0])));
        ((Blue) colors[1][1]).setUpInputBlue(((Green) colors[0][1]).getGreenOutput(chooseBlackFunc(arr[0][1])));
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        //همه آبی ها ورودی چپ اول سبزیا
        for (int i = 2; i < n - 1; i++){
            ((Blue) colors[i][1]).setLeftInputBlue(((Green) colors[i][0]).
                    getGreenOutput(chooseBlackFunc(arr[i][0])));
        }
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        //همه آبیا وروذی بالا اول سبزیا
        for (int i = 2; i < n - 1; i++){
            ((Blue) colors[1][i]).setUpInputBlue(((Green) colors[0][i]).
                    getGreenOutput(chooseBlackFunc(arr[0][i])));
        }
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        //حالا آبیا
        for (int i = 2; i < n - 1; i++){
            for (int j = 2; j < n - 1; j++){
                ((Blue) colors[i][j]).setUpInputBlue(((Blue) colors[i - 1][j]).
                        getGoDownBlue(chooseBlackFunc(arr[i - 1][j])));
                ((Blue) colors[i][j]).setLeftInputBlue(((Blue) colors[i][j - 1]).
                        getGoRightBlue(chooseBlackFunc(arr[i][j - 1])));
            }
        }
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        //صورتیا ستونی
        ((Pink) colors[1][n - 1]).setFirstInputPink(((Blue) colors[1][n - 2]).
                getGoRightBlue(chooseBlackFunc(arr[1][n - 2])));
        ((Pink) colors[1][n - 1]).setSecondInputPink(((Yellow) colors[0][n - 1]).
                getOutputYellow(chooseBlackFunc(arr[0][n - 1])));
        for (int i = 2; i < n - 1; i++){
            ((Pink) colors[i][n - 1]).setFirstInputPink(((Blue) colors[i][n - 2]).
                    getGoRightBlue(chooseBlackFunc(arr[i][n - 2])));
            ((Pink) colors[i][n - 1]).setSecondInputPink(((Pink) colors[i - 1][n - 1]).
                    getLastOutputPink(chooseWhiteFunc(arr[i - 1][n - 1])));
        }
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        //صورتیا افقی
        ((Pink) colors[n - 1][1]).setFirstInputPink(((Yellow) colors[n - 1][0]).
                getOutputYellow(chooseBlackFunc(arr[n - 1][0])));
        ((Pink) colors[n - 1][1]).setSecondInputPink(((Blue) colors[n - 2][1]).
                getGoDownBlue(chooseBlackFunc(arr[n - 2][1])));
        for (int i = 2; i < n - 1; i++){
            ((Pink) colors[n - 1][i]).setFirstInputPink(((Pink) colors[n - 1][i - 1]).
                    getLastOutputPink(chooseWhiteFunc(arr[n - 1][i - 1])));
            ((Pink) colors[n - 1][i]).setSecondInputPink(((Blue) colors[n - 2][i]).
                    getGoDownBlue(chooseBlackFunc(arr[n - 2][i])));
        }
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        ((Pink) colors[n - 1][n - 1]).setFirstInputPink(((Pink) colors[n - 1][n - 2]).
                getLastOutputPink(chooseWhiteFunc(arr[n - 1][n - 2])));
        ((Pink) colors[n - 1][n - 1]).setSecondInputPink(((Pink) colors[n - 2][n - 1]).
                getLastOutputPink(chooseWhiteFunc(arr[n - 2][n - 1])));
        return ((Pink) colors[n - 1][n - 1]).getLastOutputPink(chooseWhiteFunc(arr[n - 1][n - 1]));
    }
}
