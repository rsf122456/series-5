package sbu.cs;

public class Black5 implements BlackFunction{
    //5 هر کاراکتر یک رشته را با کاراکتر هم رشته خود از آخر حروف الفبا جابه جا میکند.
    public String func(String arg){
        String main = "abcdefghijklmnopqrstuvwxyz";
        StringBuilder stringBuilder = new StringBuilder(arg);
        outer:for (int i = 0; i < arg.length(); i++){
            for (int j = 0; j < main.length(); j++){
                if (stringBuilder.charAt(i) == main.charAt(j)){
                    stringBuilder.setCharAt(i,main.charAt(main.length() - 1 - j));
                    continue outer;
                }
            }
        }
        return stringBuilder.toString();
    }
}
