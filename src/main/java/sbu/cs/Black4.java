package sbu.cs;

public class Black4 implements BlackFunction{
    //4 یک کاراکتر رشته را به سمت راست شیفت میدهد.
    public String func(String arg){
        arg = arg.charAt(arg.length() - 1) + arg.substring(0,arg.length() - 1);
        return arg;
    }
}
