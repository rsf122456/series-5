package sbu.cs;

public class Yellow extends Colors{
    private String inputYellow;
    private BlackFunction BF;
    public void setInputYellow(String inputYellow){
        this.inputYellow = inputYellow;
    }
    public String getOutputYellow(BlackFunction BF){
        this.BF = BF;
        String outputYellow = BF.func(this.inputYellow);
        return outputYellow;
    }
}
