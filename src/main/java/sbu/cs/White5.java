package sbu.cs;

public class White5 implements WhiteFunction{
    //5 بر 26 تقسیم میکند.
    public String func(String first, String second){
        if (first.length() != second.length())
            return NotEqual(first, second);
        else
            return Equal(first, second);
    }
    private String NotEqual(String first, String second){
        StringBuilder main = new StringBuilder();
        int size = first.length() < second.length() ? first.length() : second.length();
        int bigSize = first.length() < second.length() ? second.length() : first.length();
        String bigger = first.length() < second.length() ? second : first;
        for (int i = 0; i < size; i++){
            char ch = (char)((first.charAt(i) + second.charAt(i) - 2 * 'a') % 26 + 'a');
            main.append(ch);
        }
        for (int k = size; k < bigSize; k++){
            main.append(bigger.charAt(k));
        }
        return main.toString();
    }
    private String Equal(String first, String second){
        StringBuilder main = new StringBuilder();
        for (int i = 0; i < first.length(); i++){
            char ch = (char)((first.charAt(i) + second.charAt(i) - 2 * 'a') % 26 + 'a');
            main.append(ch);
        }
        return main.toString();
    }
}
