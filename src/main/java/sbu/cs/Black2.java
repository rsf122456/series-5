package sbu.cs;

public class Black2 implements BlackFunction{
    //2 هر کاراکتر رشته را تکرار میکند.
    public String func(String arg){
        int l = arg.length();
        String newString = "";
        for (int i = 0; i < l; i++) {
            newString += arg.substring(i,i+1) + arg.substring(i, i+1);
        }
        return newString;
    }
}
