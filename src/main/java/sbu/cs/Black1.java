package sbu.cs;

public class Black1 implements BlackFunction{
    //1 وارونه رشته را بر میگرداند
    public String func(String arg){
        return new StringBuilder(arg).reverse().toString();
    }
}
