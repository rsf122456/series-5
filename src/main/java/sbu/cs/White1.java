package sbu.cs;

public class White1 implements WhiteFunction{
    //1 حروف رشته ها را بین هم قرار میدهد
    public String func(String first, String second) {
        if (first.length() != second.length())
            return NotEqual(first, second);
        else
            return Equal(first, second);
    }
    private String NotEqual(String first, String second){
        StringBuilder main = new StringBuilder();
        int size = first.length() < second.length() ? first.length() : second.length();
        int bigSize = first.length() < second.length() ?  second.length() : first.length();
        String bigger = first.length() < second.length() ?  second : first;
        int j = 0;
        int p = 0;
        for (int i = 0; i < 2 * size; i++){
            if (i % 2 == 0) {
                main.append(first.charAt(p));
                p++;
            } else {
                main.append(second.charAt(j));
                j++;
            }
        }
        for (int i = size; i < bigSize; i++){
            main.append(bigger.charAt(i));
        }
        return main.toString();
    }
    private String Equal(String first, String second){
        StringBuilder main = new StringBuilder();
        int j = 0;
        int p = 0;
        for (int i = 0; i < 2 * first.length(); i++){
            if (i % 2 == 0) {
                main.append(first.charAt(p));
                p++;
            } else {
                main.append(second.charAt(j));
                j++;
            }
        }
        return main.toString();
    }
}
